# Extreme VSP - Get Informations for Telemetry

Install Library

```shell
pip install -r requirements.txt
```

Run WEB server

```shell
» ./main.py
 * Serving Flask app "main" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://0.0.0.0:55555/ (Press CTRL+C to quit)
127.0.0.1 - - [23/May/2019 23:20:14] "GET / HTTP/1.1" 404 -
127.0.0.1 - - [23/May/2019 23:20:21] "GET /ip_interfaces HTTP/1.1" 404 -
127.0.0.1 - - [23/May/2019 23:20:33] "GET /interfaces_ip HTTP/1.1" 200 -
127.0.0.1 - - [23/May/2019 23:20:42] "GET /software_infos HTTP/1.1" 200 -
```

Execute a WEB API Call

![web_api.png](./docs/images/web_api.png)

![web_api_stats_ers.png](./docs/images/web_api_stats_ers.png)

![web_api_stats_vsp.png](./docs/images/web_api_stats_vsp.png)