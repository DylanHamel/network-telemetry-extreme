Received
    Packets:                 67328767779
    Multicasts:              3718648
    Broadcasts:              1498289
    Total Octets:            40652563483674
    Packets 64 bytes:        36555
            65-127 bytes:    33436748793
            128-255 bytes:   2704744686
            256-511 bytes:   3736729165
            512-1023 bytes:  4694109903
            1024-1518 bytes: 5734945039
            1519-2047 bytes: 17021453638
            2048-4095 bytes: 0
            4096-9216 bytes: 0
    Total Jumbo 1519-9216:   17021453638
    Control Packets:         0
    FCS Errors:              0
    Undersized Packets:      0
    Oversized Packets:       17021453638
    Filtered Packets:        0
    Pause Frames:            0
Transmitted
    Packets:                 77179278291
    Multicasts:              874333637
    Broadcasts:              406867888
    Total Octets:            43319069900817
    Packets 64 bytes:        254539039
            65-127 bytes:    36541540875
            128-255 bytes:   10385377630
            256-511 bytes:   3075956713
            512-1023 bytes:  2556826541
            1024-1518 bytes: 5302507070
            1519-2047 bytes: 19062530423
            2048-4095 bytes: 0
            4096-9216 bytes: 0
    Total Jumbo 1519-9216:   19062530423
    Oversized Packets:       19062530423
    Collisions:              0
    Single Collisions:       0
    Multiple Collisions:     0
    Excessive Collisions:    0
    Deferred Packets:        0
    Late Collisions:         0
    Pause Frames:            0
    Dropped On No Resources: 181277036
