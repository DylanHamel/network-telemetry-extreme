#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__     = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__    = "0.1"
__email__      = "dhamel@pictet.com"
__status__     = "Prototype"
__copyright__  = "Copyright 2019"
__license__    = "MIT"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("Error import [deploy_vrf.py] pprint")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import textfsm
except ImportError as importError:
    print("Error import [deploy_vrf.py] textfsm")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir import InitNornir
    
	# To use advanced filters
    from nornir.core.filter import F
	
	# To use HTTP requests
    from nornir.plugins.tasks.apis import http_method
	
	# To execute commands through SSH
    from nornir.plugins.tasks.commands import remote_command
    
	# Netmiko commands
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.plugins.tasks.networking import netmiko_save_config
    from nornir.plugins.tasks.networking import netmiko_send_config
    
	# To use PING
    from nornir.plugins.tasks.networking import tcp_ping
	
	# To print task results
    from nornir.plugins.functions.text import print_result
	
	# To retrieve device datas in a YAML file
    from nornir.plugins.tasks.data import load_yaml

    # To generate template from Jinja2
    from nornir.plugins.tasks.text import template_file

except ImportError as importError:
    print("Error import [deploy_vrf.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Constantes
#
DECAL = len("------------------")+1
VRF_NO_EXISTS = "No VRF exists with id:"

######################################################
#
# Variables
#
nr = InitNornir(
    config_file="./nornir/config.yml",
    logging={"file": "./nornir/nornir.log", "level": "debug"}
)

result = dict()
vrf_missing = dict()

vrf_to_remove = dict()

######################################################
#
# Functions
#
def get_vars(task):
	
	output = task.run(
        name="Load Host specific datas from ./data/",
        task=load_yaml,
        file=f"./data/{task.host}.yml",
    )

	task.host["vars"] = output.result

#
# Generate template From Jinja2
#
def generate_template(task):
    
    output = task.run(
        name="Generate templates from ./templates/jinja2/",
        task=template_file,
        template="vrf.j2",
        path="./templates/jinja2/"
    )
    
    file = open(f"./templates/files/{task.host.name}_vrf.cfg", "w+")
    file.write(output.result)
    file.close()

#
# NETWORK DEVICES - NETMIKO
#

def extreme_check_if_vrfid_already_exists(task):
	
	task.host["vrf_to_create"] = list()
	task.host["vrf_in_code"] = list()
	
	for vrf in task.host['vars']['vrfs']:
		
		output = task.run(
			name=f"Execute show ip vrf vrfid {vrf['vrf_id']}",
			task=netmiko_send_command,
			command_string=f"show ip vrf vrfid {vrf['vrf_id']}",
			enable=True
		)
		
		if VRF_NO_EXISTS in output.result:
			task.host["vrf_to_create"].append(vrf)
		
		task.host["vrf_in_code"].append(vrf)
	
# ----------------------------------------------------
#
#
def extreme_configure_new_vrf(task):
  
    if "vrf_to_create" in task.host.keys() and len(task.host['vrf_to_create']) > 0:
        output = task.run(
            name=f"Execute commands in templates/files/{task.host.name}_vrf.cfg",
            task=netmiko_send_config,
            config_file=f"./templates/files/{task.host.name}_vrf.cfg"
        )
	
# ----------------------------------------------------
#
#
def extreme_show_ip_vrf(task):

    output = task.run(
        name="Execute show ip vrf",
        task=netmiko_send_command,
        command_string="show ip vrf",
        enable=True
    )

    template = open(
        './templates/textfsm/extreme_vsp_show_ip_vrf.template')
    results_template = textfsm.TextFSM(template)

    parsed_results = results_template.ParseText(output.result)
    result[task.host.name] = list()

    for line in parsed_results:
        tmp = dict()
        tmp["vrf_name"] = line[0]
        tmp["vrf_id"] = line[1]
        tmp["vlan_count"] = line[2]
        tmp["arp_count"] = line[3]
        result[task.host.name].append(tmp)
		
# ----------------------------------------------------
#
#
def verify_all_vlans_exist(task):
    
    lst_vrf_ids_in_config = extract_vrfid_from_output(result[task.host.name])
    lst_vrf_ids_in_code = extract_vrfid_from_output(task.host['vrf_in_code'])

    vrf_missing = list()

    for vrf_in_conf in lst_vrf_ids_in_config:
        if str(vrf_in_conf) not in lst_vrf_ids_in_code:
            for vrf_to_remove in result[task.host.name]:
                if str(vrf_to_remove['vrf_id']) == str(vrf_in_conf):
                    vrf_missing.append(vrf_to_remove['vrf_name'])
            
    task.host["vrfs_to_remove"] = vrf_missing

# ----------------------------------------------------
#
#
def extreme_remove_vrf(task):
    
    print("[extreme_remove_vrf] -", task.host["vrfs_to_remove"])

    if "vrfs_to_remove" in task.host.keys() and len(task.host["vrfs_to_remove"]) > 0:
        
        output = task.run(
            name="Generate templates from ./templates/jinja2/remove_vrf.j2",
            task=template_file,
            template="remove_vrf.j2",
            path="./templates/jinja2/"
        )

        output = task.run(
            name="Remove ip vrf",
            task=netmiko_send_config,
            config_commands=output.result
        )
	
# ----------------------------------------------------
#
#
def extract_vrfid_from_output(show_ip_vrf_output:list()) -> list():
	
	lst_vrf_ids = list()
	
	for vrf_infos in show_ip_vrf_output:
		lst_vrf_ids.append(str(vrf_infos['vrf_id']))
	
	return lst_vrf_ids
			
######################################################
#
# MAIN Functions
#
def main():
    
    devices = nr.filter(F(groups__contains="switch"))
    print(devices.inventory.hosts)
	
	# print(devices.inventory.hosts['sw01'].platform)
	# devices.inventory.add_host("sw02")
	
	
	# Retrieve vars
    data = devices.run(
        task=get_vars,
        on_failed=True
    )
    print_result(data)
	
	
	# Check if VRF already exists on VSP
    data = devices.run(
        task=extreme_check_if_vrfid_already_exists,
        on_failed=True
    )
    print_result(data)
	
    # Generate config from Jinja2
    data = devices.run(
        task=generate_template,
        on_failed=True
    )
    print_result(data)
	
	# Create VRF on devices
    data = devices.run(
        task=extreme_configure_new_vrf,
        on_failed=True
    )
    print_result(data)

    # Show all VRFs on devices
    data = devices.run(
        task=extreme_show_ip_vrf,
        on_failed=True
    )
    print_result(data)

	# Show all VRFs on devices
    data = devices.run(
        task=verify_all_vlans_exist,
        on_failed=True
    )
    print_result(data)

    # Remove VRFs that are not in yaml
    data = devices.run(
        task=extreme_remove_vrf,
        on_failed=True
    )
    print_result(data)
	
	# Show all VRFs on devices
    data = devices.run(
        task=extreme_show_ip_vrf,
        on_failed=True
    )
    print_result(data)
    
    PP.pprint(result)
	
    print("------------------------------------------------ \n main finished...")
    exit(EXIT_SUCCESS)

# ----------------------------------------------------
#
#
if __name__ == "__main__":
    main()
