#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__     = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__    = "0.1"
__email__      = "dylan.hamel@protonmail.com"
__status__     = "Prototype"
__copyright__  = "Copyright 2019"
__license__    = "MIT"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import logging
except ImportError as importError:
    print("Error import [extreme-json - main] logging")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json
except ImportError as importError:
    print("Error import [extreme-json - main] json")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import textfsm
except ImportError as importError:
    print("Error import [extreme-json - main] textfsm")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir import InitNornir
    from nornir.plugins.tasks.commands import remote_command
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.plugins.tasks.networking import netmiko_save_config
    from nornir.plugins.tasks.networking import netmiko_send_config
    from nornir.plugins.functions.text import print_result

except ImportError as importError:
    print("Error import [extreme-json - main] nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from flask import Flask, Response, request
    
except ImportError as importError:
    print("Error import [extreme-json - main] flask")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import netmiko

except ImportError as importError:
    print("Error import [extreme-json - main] netmiko")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("Error import [eveng-api] pprint")
    print(importError)
    exit(EXIT_FAILURE)
######################################################
#
# Constantes
#
DECAL = len("------------------")+1
VRF_NO_EXISTS = "No VRF exists with id:"

######################################################
#
# Variables
#
nr = InitNornir(
    config_file="config.yml", 
    logging={"file": ".inventory/mylogs", "level": "debug"}
)

result = dict()

######################################################
#
# Functions
#
def _extreme_ers_get_port_statistics_to_json(data, port):
    index = data.find("Transmitted")
    outputRX = data[:index-1]
    outputTX = data[index:]

    templateRX = open(
      'templates/textfsm/extreme_ers_show_port_statistics_rx.template')
    templateTX = open(
      'templates/textfsm/extreme_ers_show_port_statistics_tx.template')

    resultsTX = textfsm.TextFSM(templateTX)
    resultsRX = textfsm.TextFSM(templateRX)

    parsedTX = resultsTX.ParseText(outputTX)
    parsedRX = resultsRX.ParseText(outputRX)

    result[port] = dict()
    result[port]['Received'] = dict()
    result[port]['Transmitted'] = dict()

    for line in parsedRX:
        result[port]['Received']['packets'] = line[0]
        result[port]['Received']['multicasts'] = line[1]
        result[port]['Received']['broadcasts'] = line[2]
        result[port]['Received']['total_octets'] = line[3]
        result[port]['Received']['packets_64_bytes'] = line[4]
        result[port]['Received']['packets_65_127'] = line[5]
        result[port]['Received']['packets_128_255'] = line[6]
        result[port]['Received']['packets_256_511'] = line[7]
        result[port]['Received']['packets_512_1023'] = line[8]
        result[port]['Received']['packets_1024_1518'] = line[9]
        result[port]['Received']['packets_1519_2047'] = line[10]
        result[port]['Received']['packets_2048_4095'] = line[11]
        result[port]['Received']['packets_4096_9216'] = line[12]
        result[port]['Received']['total_jumbo'] = line[13]
        result[port]['Received']['contro_packets'] = line[14]
        result[port]['Received']['fcs_errors'] = line[15]
        result[port]['Received']['undersized_packets'] = line[16]
        result[port]['Received']['oversized_packets'] = line[17]
        result[port]['Received']['filtered_packets'] = line[18]
        result[port]['Received']['pause_frames'] = line[19]

    for line in parsedTX:
        result[port]['Transmitted']['packets'] = line[0]
        result[port]['Transmitted']['multicasts'] = line[1]
        result[port]['Transmitted']['broadcasts'] = line[2]
        result[port]['Transmitted']['total_octets'] = line[3]
        result[port]['Transmitted']['packets_64_bytes'] = line[4]
        result[port]['Transmitted']['packets_65_127'] = line[5]
        result[port]['Transmitted']['packets_128_255'] = line[6]
        result[port]['Transmitted']['packets_256_511'] = line[7]
        result[port]['Transmitted']['packets_512_1023'] = line[8]
        result[port]['Transmitted']['packets_1024_1518'] = line[9]
        result[port]['Transmitted']['packets_1519_2047'] = line[10]
        result[port]['Transmitted']['packets_2048_4095'] = line[11]
        result[port]['Transmitted']['packets_4096_9216'] = line[12]
        result[port]['Transmitted']['total_jumbo'] = line[13]
        result[port]['Transmitted']['oversized_packets'] = line[14]
        result[port]['Transmitted']['collisions'] = line[15]
        result[port]['Transmitted']['single_collisions'] = line[16]
        result[port]['Transmitted']['multiple_collisions'] = line[17]
        result[port]['Transmitted']['excessive_collisions'] = line[18]
        result[port]['Transmitted']['deferred_packets'] = line[19]
        result[port]['Transmitted']['late_collisions'] = line[20]
        result[port]['Transmitted']['pause_frames'] = line[21]
        result[port]['Transmitted']['dropped_on_no_resources'] = line[22]


def convert_result(result):
    return "\n".join([r.result for r in result.values()])


def _extreme_vsp_get_port_statistics(task):

    output = task.run(
        name="Execute show int gi statis",
        task=netmiko_send_command,
        command_string="show interfaces gigabitEthernet statistics",
        enable=True
    )

    output = output.result
    
    template = open(
        'templates/textfsm/extreme_vsp_show_int_giga_statistics.template')

    resultFSM = textfsm.TextFSM(template)
    parsed = resultFSM.ParseText(output)

    for line in parsed:
        if line[0] not in result.keys():
            result[line[0]] = dict()
            result[line[0]]['in_octets'] = line[1]
            result[line[0]]['out_octets'] = line[2]
            result[line[0]]['in_packets'] = line[3]
            result[line[0]]['out_packets'] = line[4]

        else:
            result[line[0]]['in_flowctrl'] = line[1]
            result[line[0]]['out_flowctrl'] = line[2]
            result[line[0]]['in_pfc'] = line[3]
            result[line[0]]['out_pfc'] = line[4]
            result[line[0]]['outloss_packets'] = line[5]


def _extreme_ers_get_port_statistics(task):
    """
    Nornir job to gather the metrics port statistics
    Transform it to JSON
    """

    #utput = task.run(
    #    name="Execute show ip interfaces",
    #    task=netmiko_send_command,
    #    command_string="show ip interface",
    #    enable=True

    #output = output.result

    output = open('result/show_ports_statistics.txt')
    output = output.read()

    while output.find("------------------") != -1:
        index = (output.find("------------------"))
        #print(output[:index-1].rfind('\n'))
        #if output[:index-1].rfind('\n') == -1:
        #  start = 0
        #else:
        #  start = output[:index-1].rfind('\n')

        port = output[:index-1]
        if port[0] == '\n':
            port = port[1:]

        if output[index+DECAL:].find("------------------") == -1:
            end = len(output[index+DECAL:])
        else:
            ref = output[index+DECAL:].find("------------------")
            end = output[index+DECAL:index+DECAL+ref-1].rfind('\n')

        _extreme_ers_get_port_statistics_to_json(output[index+DECAL:index+DECAL+end], port)

        output = output[index+DECAL+end:]





def _get_interfaces_ip(task):
    """
    Nornir job to gather the metrics using `napalm_get` task
    and transform napalm metrics into prometheus metrics.
    """

    output = task.run(
        name="Execute show ip interfaces",
        task=netmiko_send_command,
        command_string="show ip interface",
        enable=True 
    )
    
    template = open(
        './templates/textfsm/extreme_vsp_show_ip_interface.template')
    results_template = textfsm.TextFSM(template)

    parsed_results = results_template.ParseText(output.result)

    result[task.host.name] = dict()

    for line in parsed_results:
        result[task.host.name][line[0]] = dict()
        result[task.host.name][line[0]]["ip_address"] = line[1]
        result[task.host.name][line[0]]["netmask"] = line[2]
        result[task.host.name][line[0]]["mtu"] = line[3]
        result[task.host.name][line[0]]["vlan_id"] = line[4]


def _get_software_infos(task):
    """
    Nornir job to gather the metrics using `napalm_get` task
    and transform napalm metrics into prometheus metrics.
    """

    r1 = task.run(
        name="Execute show software",
        task=netmiko_send_command,
        command_string="show software",
        enable=True
    )

# ----------------------------------------------------
#
#
def extreme_vsp_check_if_vrfid_already_exists(task, vrfid):

    output = task.run(
        name=f"Execute show ip vrf vrfid {vrfid}",
        task=netmiko_send_command,
        command_string=f"show ip vrf vrfid {vrfid}",
        enable=True
    )

    if VRF_NO_EXISTS in output.result:
        task.host["vrf_exists"] = False
    else:
        task.host["vrf_exists"] = True

# ----------------------------------------------------
#
#
def extreme_vsp_configure_new_vrf(task):

    file = open("config/config_vrf.txt", "r")
    file_data = file.read()

    if "vrf_exists" in task.host.keys():
        if task.host["vrf_exists"] is False:
            output = task.run(
                name="Execute config/config_vrf.txt",
                task=netmiko_send_config,
                config_file="config/config_vrf.txt"
            )

# ----------------------------------------------------
#
#
def extreme_vsp_show_ip_vrf(task):

    output = task.run(
        name="Execute show ip vrf",
        task=netmiko_send_command,
        command_string="show ip vrf",
        enable=True
    )

    template = open(
        './templates/textfsm/extreme_vsp_show_ip_vrf.template')
    results_template = textfsm.TextFSM(template)

    parsed_results = results_template.ParseText(output.result)
    result[task.host.name] = list()

    for line in parsed_results:
        tmp = dict()
        tmp["vrf_name"] = line[0]
        tmp["vrf_id"] = line[1]
        tmp["vlan_count"] = line[2]
        tmp["arp_count"] = line[3]

        result[task.host.name].append(tmp)


######################################################
#
# MAIN Functions
#
def main():
    

    devices = nr.filter(role="switch_access")
    
    print(devices.inventory.hosts)


    data = devices.run(
        task=extreme_vsp_check_if_vrfid_already_exists,
        vrfid="11",
        on_failed=True
    )
    print_result(data)

    data = devices.run(
        task=extreme_vsp_configure_new_vrf,
        on_failed=True
    )
    print_result(data)

    data = devices.run(
        task=extreme_vsp_show_ip_vrf,
        on_failed=True
    )
    print_result(data)

    PP.pprint(result)
        
    print("------------------------------------------------ \n main finished...")
    exit(EXIT_SUCCESS)


######################################################
#
# WEB API
#
app = Flask(__name__)

@app.route("/interfaces_ip", methods=['GET'])
def interfaces_ip():
    """
    /interfaces_ip endpoint
    Gather metrics from the network and presents it to prometheus

    http://127.0.0.1/interfaces_ip?devices=spine01&site=ch.gva
    http://127.0.0.1/interfaces_ip?devices=spine01
    http://127.0.0.1/interfaces_ip?site=ch.gva
   
    """

    host_url = request.args.get('host')
    
    if host_url is not None:
        devices = nr.filter(host=host_url)
    else:
        devices = nr.filter(all="all")

    data = devices.run(
        task=_get_interfaces_ip,
        on_failed=True,
        num_workers=1
    )

    return Response(
        json.dumps(result),
        status=200,
        mimetype="application/json"
    )
    


@app.route("/software_infos", methods=['GET'])
def software_infos():
    """
    /software_infos endpoint
    Gather metrics from the network and presents it to prometheus
    """
    results = nr.run(
        task=_get_software_infos,
        on_failed=True,
    )

    results = convert_result(results)

    return Response(
        json.dumps(result),
        status=200,
        mimetype="application/json"
    )

@app.route("/ers_port_statistics", methods=['GET'])
def ers_port_statistics():
    """
    /port_statistics endpoint
    Gather metrics from the network and presents it to prometheus
    """

    results = nr.run(
        task=_extreme_ers_get_port_statistics,
        on_failed=True,
    )

    return Response(
        json.dumps(result),
        status=200,
        mimetype="application/json"
    )
    

@app.route("/vsp_port_statistics", methods=['GET'])
def vsp_port_statistics():
    """
    /port_statistics endpoint
    Gather metrics from the network and presents it to prometheus
    """

    host_url = request.args.get('type')

    if host_url is not None:
        devices = nr.filter(type=host_url)
    else:
        devices = nr.filter(maker="extreme_vsp")

    results = nr.run(
        task=_extreme_vsp_get_port_statistics,
        on_failed=True,
    )
    print(results.output)

    return Response(
        json.dumps(results),
        status=200,
        mimetype="application/json"
    )

# -----------------------------------------------------------------------------------------------------------------------------
#
#
if __name__ == "__main__":
    main()
    #app.run(host="0.0.0.0", port=55555, debug=True)
