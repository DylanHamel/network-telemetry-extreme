#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "0.1"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"
__license__ = "MIT"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import textfsm
except ImportError as importError:
    print("Error import [test] textfsm")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("Error import [test] pprint")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Constantes
#

DECAL = len("------------------")+1

######################################################
#
# Variables
#


def to_json(data, port):

  index = data.find("Transmitted")
  outputRX = data[:index-1]
  outputTX = data[index:]

  templateRX = open(
      'templates/textfsm/extreme_ers_show_port_statistics_rx.template')
  templateTX = open(
      'templates/textfsm/extreme_ers_show_port_statistics_tx.template')

  resultsTX = textfsm.TextFSM(templateTX)
  resultsRX = textfsm.TextFSM(templateRX)

  parsedTX = resultsTX.ParseText(outputTX)
  parsedRX = resultsRX.ParseText(outputRX)

  result = dict()
  result[port] = dict()
  result[port]['Received'] = dict()
  result[port]['Transmitted'] = dict()

  for line in parsedRX:
    result[port]['Received']['packets'] = line[0]
    result[port]['Received']['multicasts'] = line[1]
    result[port]['Received']['total_octets'] = line[2]
    result[port]['Received']['packets_64_bytes'] = line[3]
    result[port]['Received']['packets_65_127'] = line[4]
    result[port]['Received']['packets_128_255'] = line[5]
    result[port]['Received']['packets_256_511'] = line[6]
    result[port]['Received']['packets_512_1023'] = line[7]
    result[port]['Received']['packets_1024_1518'] = line[8]
    result[port]['Received']['packets_1519_2047'] = line[9]
    result[port]['Received']['packets_2048_4095'] = line[10]
    result[port]['Received']['packets_4096_9216'] = line[11]
    result[port]['Received']['total_jumbo'] = line[12]
    result[port]['Received']['contro_packets'] = line[13]
    result[port]['Received']['fcs_errors'] = line[14]
    result[port]['Received']['undersized_packets'] = line[15]
    result[port]['Received']['oversized_packets'] = line[16]
    result[port]['Received']['filtered_packets'] = line[17]
    result[port]['Received']['pause_frames'] = line[18]

  for line in parsedTX:
    result[port]['Transmitted']['packets'] = line[0]
    result[port]['Transmitted']['multicasts'] = line[1]
    result[port]['Transmitted']['total_octets'] = line[2]
    result[port]['Transmitted']['packets_64_bytes'] = line[3]
    result[port]['Transmitted']['packets_65_127'] = line[4]
    result[port]['Transmitted']['packets_128_255'] = line[5]
    result[port]['Transmitted']['packets_256_511'] = line[6]
    result[port]['Transmitted']['packets_512_1023'] = line[7]
    result[port]['Transmitted']['packets_1024_1518'] = line[8]
    result[port]['Transmitted']['packets_1519_2047'] = line[9]
    result[port]['Transmitted']['packets_2048_4095'] = line[10]
    result[port]['Transmitted']['packets_4096_9216'] = line[11]
    result[port]['Transmitted']['total_jumbo'] = line[12]
    result[port]['Transmitted']['oversized_packets'] = line[13]
    result[port]['Transmitted']['collisions'] = line[14]
    result[port]['Transmitted']['single_collisions'] = line[15]
    result[port]['Transmitted']['multiple_collisions'] = line[16]
    result[port]['Transmitted']['excessive_collisions'] = line[17]
    result[port]['Transmitted']['deferred_packets'] = line[18]
    result[port]['Transmitted']['late_collisions'] = line[19]
    result[port]['Transmitted']['pause_frames'] = line[20]
    result[port]['Transmitted']['dropped_on_no_resources'] = line[21]

  PP.pprint(result)


output = open('result/show_int_gi_statistics.txt')
output = output.read()

template = open(
    'templates/textfsm/extreme_vsp_show_int_giga_statistics.template')

result = textfsm.TextFSM(template)
parsed = result.ParseText(output)

result = dict()

for line in parsed:
  if line[0] not in result.keys():
    result[line[0]] = dict()
    result[line[0]]['in_octets'] = line[1]
    result[line[0]]['out_octets'] = line[2]
    result[line[0]]['in_packets'] = line[3]
    result[line[0]]['out_packets'] = line[4]
  
  else:
    result[line[0]]['in_flowctrl'] = line[1]
    result[line[0]]['out_flowctrl'] = line[2]
    result[line[0]]['in_pfc'] = line[3]
    result[line[0]]['out_pfc'] = line[4]
    result[line[0]]['outloss_packets'] = line[5]

    

PP.pprint(result)

"""
output = open('result/show_ports_statistics1.txt')
output = output.read()

while output.find("------------------") != -1:

  index = (output.find("------------------"))
  #print(output[:index-1].rfind('\n'))
  #if output[:index-1].rfind('\n') == -1:
  #  start = 0
  #else:
  #  start = output[:index-1].rfind('\n')

  port = output[:index-1]
  if port[0] == '\n':
    port = port[1:]

  if output[index+DECAL:].find("------------------") == -1:
    end = len(output[index+DECAL:])
  else:
    ref = output[index+DECAL:].find("------------------")
    end = output[index+DECAL:index+DECAL+ref-1].rfind('\n')
  
  to_json(output[index+DECAL:index+DECAL+end], port)
  
  output = output[index+DECAL+end:]
"""
