#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
This script will retrieve some port statistics (29 values).
To run => ./get_port_statistics.py

It's an alternative to SNMP polling.
With SNMP, you will perform 44 SNMP_GET per port.

There are 82 ports an VSP 8K

=> env. 3600 SNMP_GET ...

This tool will only perform 7 (!) SSH commands.

"""

__author__     = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__    = "1.0"
__email__      = "dylan.hamel@protonmail.com"
__status__     = "Production"
__copyright__  = "Copyright 2019"
__license__    = "MIT"


######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1


######################################################
#
# Import Library
#
try:
    import netmiko
except ImportError as importError:
    print("Error import [main] netmiko")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import textfsm
except ImportError as importError:
    print("Error import [main] textfsm")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("Error import [main] pprint")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import click
except ImportError as importError:
    print("Error import [main] click")
    print(importError)
    exit(EXIT_FAILURE)

###################################################
#
# Constantes
#
INTERFACES_TO_MONITOR = "1/1"
DEVICE_TO_MONITOR = ["core01", "core02", "core03", "core04"]
COMMANDS_TO_EXECUTE = {"statistics": "show interfaces gigabitEthernet statistics",
        "error": "show interfaces gigabitEthernet error",
        "vlacp": "show interfaces gigabitEthernet statistics vlacp",
        "verbose": "show interfaces gigabitEthernet verbose",
        "state": "show interfaces gigabitEthernet state",
        "l1-int": "show interfaces gigabitEthernet l1-interface",
        "interface": "show interfaces gigabitEthernet interface"
}

TEXTFSM_PATH = "./templates/textfsm/"
JINJA2_PATH = "./templates/jinja2/"
DATA_VSP_PATH = "./data/extreme_vsp/"


######################################################
#
# Functions
#
def open_template(template_name:str()) -> list():

        template = open(TEXTFSM_PATH+template_name)
        results_template = textfsm.TextFSM(template)

        parsed_results = results_template.ParseText(output)
        result[device] = dict()


def open_template_with_data(template_name:str(), output_file:str(), * , mode="r") -> list():

        file = open(DATA_VSP_PATH+output_file, mode)
        output = file.read()


        template = open(TEXTFSM_PATH+template_name)
        results_template = textfsm.TextFSM(template)

        parsed_results = results_template.ParseText(output)
        return parsed_results

######################################################
#
# MAIN Functions
#
@click.command()
@click.option('--port', default="#", help='Port that you want value.')
def main(port):
        result = dict()
        for device in DEVICE_TO_MONITOR:
        #extreme_netmiko = {
        #    'device_type': 'extreme_vsp',
        #    'username': "netprod",
        #    'password': "abcdefghikl-password-to-set:)",
        #    'host': device
        #}

        # output = extreme_netmiko.send_command(COMMANDS_TO_EXECUTE.format(INTERFACES_TO_MONITOR))

                result[device] = dict()


                #output_cmd = extreme_netmiko.send_command(COMMANDS_TO_EXECUTE['statistics'])
                #parsed_results = open_template_with_data("extreme_vsp_show_int_giga_statistics.template", output_cmd)

                parsed_results = open_template_with_data("extreme_vsp_show_int_giga_statistics.template", "output_show_int_gi_statistics.txt")
                parsed_extreme_vsp_show_int_gi_statistics(result, device, parsed_results)

                parsed_results = open_template_with_data("extreme_vsp_show_int_giga_error.template", "output_show_int_gi_error.txt")
                parsed_extreme_vsp_show_int_gi_error(result, device, parsed_results)

                parsed_results = open_template_with_data("extreme_vsp_show_int_giga_statistics_vlacp.template", "output_show_int_gi_statistics_vlacp.txt")
                parsed_extreme_vsp_show_int_gi_statistics_vlacp(result, device, parsed_results)

                parsed_results = open_template_with_data("extreme_vsp_show_int_giga_statistics_verbose.template", "output_show_int_gi_statistics_verbose.txt")
                parsed_extreme_vsp_show_int_gi_statistics_verbose(result, device, parsed_results)

                parsed_results = open_template_with_data("extreme_vsp_show_int_giga_state.template", "output_show_int_gi_state.txt")
                parsed_extreme_vsp_show_int_gi_state(result, device, parsed_results)

                parsed_results = open_template_with_data("extreme_vsp_show_int_giga_l1-interface.template", "output_show_int_gi_l1-interface.txt")
                parsed_extreme_vsp_show_int_gi_l1_interface(result, device, parsed_results)

                parsed_results = open_template_with_data("extreme_vsp_show_int_giga_interface.template", "output_show_int_gi_interface.txt")
                parsed_extreme_vsp_show_int_gi_interface(result, device, parsed_results)

                break

        if port is "#":
                PP.pprint(result)
        else:
                for d in DEVICE_TO_MONITOR:
                        PP.pprint(result[d][port])
                        exit(0)


# -----------------------------------------------------------------------------------------------------------------------------
#
#

def parsed_extreme_vsp_show_int_gi_statistics(result:dict(), device_name:str(), parsed_results:list()):

        for line in parsed_results:
                if line[0] not in result[device_name].keys():
                        result[device_name][line[0]] = dict()
                        result[device_name][line[0]]['in_octets'] = line[1]
                        result[device_name][line[0]]['out_octets'] = line[2]
                        result[device_name][line[0]]['in_packets'] = line[3]
                        result[device_name][line[0]]['out_packets'] = line[4]

                else:
                        result[device_name][line[0]]['in_flowctrl'] = line[1]
                        result[device_name][line[0]]['out_flowctrl'] = line[2]
                        result[device_name][line[0]]['in_pfc'] = line[3]
                        result[device_name][line[0]]['out_pfc'] = line[4]
                        result[device_name][line[0]]['outloss_packets'] = line[5]


def parsed_extreme_vsp_show_int_gi_error(result:dict(), device_name:str(), parsed_results:list()):

        for line in parsed_results:
                result[device_name][line[0]]['error_align'] = line[1]
                result[device_name][line[0]]['error_fcs'] = line[2]
                result[device_name][line[0]]['frames_long'] = line[3]
                result[device_name][line[0]]['too_short'] = line[4]
                result[device_name][line[0]]['link_failure'] = line[5]
                result[device_name][line[0]]['carrier_sense'] = line[6]
                result[device_name][line[0]]['carrier_errors'] = line[7]
                result[device_name][line[0]]['sqtest_errors'] = line[8]
                result[device_name][line[0]]['in_discard'] = line[9]


def parsed_extreme_vsp_show_int_gi_statistics_vlacp(result:dict(), device_name:str(), parsed_results:list()):

        for line in parsed_results:
                result[device_name][line[0]]['vlacp_tx_vlacpdu'] = line[1]
                result[device_name][line[0]]['vlacp_rx_vlacpdu'] = line[2]
                result[device_name][line[0]]['vlacp_seqnum_mismatch'] = line[3]


def parsed_extreme_vsp_show_int_gi_statistics_verbose(result:dict(), device_name:str(), parsed_results:list()):

        for line in parsed_results:
                result[device_name][line[0]]['in_unicast'] = line[1]
                result[device_name][line[0]]['out_unicast'] = line[2]
                result[device_name][line[0]]['in_multicast'] = line[3]
                result[device_name][line[0]]['out_multicast'] = line[4]
                result[device_name][line[0]]['in_brdcst'] = line[5]
                result[device_name][line[0]]['out_brdcst'] = line[6]
                result[device_name][line[0]]['in_lsm'] = line[7]
                result[device_name][line[0]]['out_lsm'] = line[8]


def parsed_extreme_vsp_show_int_gi_state(result:dict(), device_name:str(), parsed_results:list()):

        for line in parsed_results:
                result[device_name][line[0]]['admin_status'] = line[1]
                result[device_name][line[0]]['port_state'] = line[2]
                result[device_name][line[0]]['state_reason'] = line[3]
                result[device_name][line[0]]['uptime_date'] = line[4]


def parsed_extreme_vsp_show_int_gi_l1_interface(result:dict(), device_name:str(), parsed_results:list()):

        for line in parsed_results:
                result[device_name][line[0]]['auto_negotiate'] = line[1]
                result[device_name][line[0]]['duplex_admin'] = line[2]
                result[device_name][line[0]]['speed_admin'] = line[3]
                result[device_name][line[0]]['duplex_operate'] = line[4]
                result[device_name][line[0]]['speed_operate'] = line[5]

def parsed_extreme_vsp_show_int_gi_interface(result:dict(), device_name:str(), parsed_results:list()):

        for line in parsed_results:
                result[device_name][line[0]]['interface_index'] = line[1]
                result[device_name][line[0]]['description_type'] = line[2]
                result[device_name][line[0]]['link_trap'] = line[3]
                result[device_name][line[0]]['port_lock'] = line[4]
                result[device_name][line[0]]['mtu'] = line[5]
                result[device_name][line[0]]['mac_address'] = line[6]


# -----------------------------------------------------------------------------------------------------------------------------
#
#
if __name__ == "__main__":
    main()
    #app.run(host="0.0.0.0", port=55555, debug=True)
