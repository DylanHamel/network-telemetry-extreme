#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Add a description ....

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"


######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import datetime
except ImportError as importError:
    print("Error import [vsp_metrics.py] datetime")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from jsonmerge import merge
except ImportError as importError:
    print("Error import [vsp_metrics.py] jsonmerge")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("Error import [vsp_metrics.py] pprint")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import parsers.vsp_parser as vp
except ImportError as importError:
    print("Error import [vsp_metrics.py] parsers.parsers")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir.plugins.tasks.networking import netmiko_send_command
except ImportError as importError:
    print("Error import [nornir_fct.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Constantes
#
METRIC_RESULTS = 'metric_results'

SHOW_INT_GI = "show interfaces gigabitEthernet"
SHOW_INT_GI_VERB = "show interfaces gigabitEthernet verbose"
SHOW_INT_GI_ERROR = "show int gigabitEthernet error"
SHOW_INT_GI_STATE = "show interfaces gigabitEthernet state"
SHOW_INT_GI_INT = "show interfaces gigabitEthernet interface"
SHOW_INT_GI_L1_INT = "show interfaces gigabitEthernet l1-interface"
SHOW_INT_GI_STATS = "show interfaces gigabitEthernet statistics"
SHOW_INT_GI_STATS_VERB = "show interfaces gigabitEthernet statistics verbose"
SHOW_INT_GI_STATS_VLACP = "show interfaces gigabitEthernet statistics vlacp"

######################################################
#
# !!!!!!!!!!!!!!! Nornir Functions !!!!!!!!!!!!!!!
#
# show interfaces gigabitEthernet error
#
def _int_errors(task):

    time_before = datetime.datetime.now().time()
    output = task.run(
        name=f"Execute {SHOW_INT_GI_ERROR}",
        task=netmiko_send_command,
        command_string=f"{SHOW_INT_GI_ERROR}",
        enable=True
    )
    time_after = datetime.datetime.now().time()

    print(f"[{task.host.name}(time_before)] - Time before command was {time_before}, time after command was {time_after}")

    error_metrics = vp.parsed_extreme_vsp_show_int_gi_error(
        output.result)

    if METRIC_RESULTS in task.host.keys():
        task.host[METRIC_RESULTS] = merge(
            task.host[METRIC_RESULTS], error_metrics)
    else:
        task.host[METRIC_RESULTS] = error_metrics

# ----------------------------------------------------
#
# show interfaces gigabitEthernet
#
def _int_std(task):
    raise NotImplementedError

# ----------------------------------------------------
#
# show interfaces gigabitEthernet state
#
def _int_state(task):
    raise NotImplementedError

# ----------------------------------------------------
#
# show interfaces gigabitEthernet statistics
#
def _int_stats(task):
    time_before = datetime.datetime.now().time()
    output = task.run(
        name=f"Execute {SHOW_INT_GI_STATS}",
        task=netmiko_send_command,
        command_string=f"{SHOW_INT_GI_STATS}",
        enable=True
    )
    time_after = datetime.datetime.now().time()
    print(f"[{task.host.name}(_int_stats)] - Time before command was {time_before}, time after command was {time_after}")

    std_metric = vp.parsed_extreme_vsp_show_int_gi_statistics(
        output.result)

    if METRIC_RESULTS in task.host.keys():
        task.host[METRIC_RESULTS] = merge(
            task.host[METRIC_RESULTS], std_metric)
    else:
        task.host[METRIC_RESULTS] = std_metric

# ----------------------------------------------------
#
# show interfaces gigabitEthernet statistics verbose
#
def _int_stats_verbose(task):
    time_before = datetime.datetime.now().time()
    output = task.run(
        name=f"Execute {SHOW_INT_GI_STATS_VERB}",
        task=netmiko_send_command,
        command_string=f"{SHOW_INT_GI_STATS_VERB}",
        enable=True
    )
    time_after = datetime.datetime.now().time()
    print(f"[{task.host.name}(_int_stats_verbose)] - Time before command was {time_before}, time after command was {time_after}")

    verbose_metric = vp.parsed_extreme_vsp_show_int_gi_statistics_verbose(
        output.result)

    if METRIC_RESULTS in task.host.keys():
        task.host[METRIC_RESULTS] = merge(
            task.host[METRIC_RESULTS], verbose_metric)
    else:
        task.host[METRIC_RESULTS] = verbose_metric


# ----------------------------------------------------
#
# show interfaces gigabitEthernet statistics vlacp
#
def _int_vlacp_stats(task):
    time_before = datetime.datetime.now().time()
    output = task.run(
        name=f"Execute {SHOW_INT_GI_STATS_VLACP}",
        task=netmiko_send_command,
        command_string=f"{SHOW_INT_GI_STATS_VLACP}",
        enable=True
    )
    time_after = datetime.datetime.now().time()
    print(f"[{task.host.name}(_int_vlacp_stats)] - Time before command was {time_before}, time after command was {time_after}")
    
    vlacp_metric = vp.parsed_extreme_vsp_show_int_gi_statistics_vlacp(output.result)

    if METRIC_RESULTS in task.host.keys():
        task.host[METRIC_RESULTS] = merge(
            task.host[METRIC_RESULTS], vlacp_metric)
    else:
        task.host[METRIC_RESULTS] = vlacp_metric

# ----------------------------------------------------
#
# show interfaces gigabitEthernet interface
#
def _int_int(task):
    time_before = datetime.datetime.now().time()
    output = task.run(
        name=f"Execute {SHOW_INT_GI_INT}",
        task=netmiko_send_command,
        command_string=f"{SHOW_INT_GI_INT}",
        enable=True
    )
    time_after = datetime.datetime.now().time()
    print(f"[{task.host.name}(_int_int)] - Time before command was {time_before}, time after command was {time_after}")

    int_metric = vp.parsed_extreme_vsp_show_int_gi_interface(
        output.result)

    if METRIC_RESULTS in task.host.keys():
        task.host[METRIC_RESULTS] = merge(
            task.host[METRIC_RESULTS], int_metric)
    else:
        task.host[METRIC_RESULTS] = int_metric

# ----------------------------------------------------
#
# show interfaces gigabitEthernet l1-interface
#
def _int_l1_int(task):
    time_before = datetime.datetime.now().time()
    output = task.run(
        name=f"Execute {SHOW_INT_GI_L1_INT}",
        task=netmiko_send_command,
        command_string=f"{SHOW_INT_GI_L1_INT}",
        enable=True
    )
    time_after = datetime.datetime.now().time()
    print(f"[{task.host.name}(_int_int)] - Time before command was {time_before}, time after command was {time_after}")

    int_l1_metric = vp.parsed_extreme_vsp_show_int_gi_l1_interface(
        output.result)

    if METRIC_RESULTS in task.host.keys():
        task.host[METRIC_RESULTS] = merge(
            task.host[METRIC_RESULTS], int_l1_metric)
    else:
        task.host[METRIC_RESULTS] = int_l1_metric

# ----------------------------------------------------
#
# Retrieve all interfaces metrics
#
def _int_all(task):
    time_before = datetime.datetime.now().time()
    _int_errors(task)
    _int_vlacp_stats(task)
    time_after = datetime.datetime.now().time()
    print(f"[{task.host.name}(int_all)] - Time before command was {time_before}, time after command was {time_after}")


# ----------------------------------------------------
#
# Generic functions
#
def _execute_cmd_vsp(task, command, message):
    time_before = datetime.datetime.now().time()
    output = task.run(
        name=f"Execute {command}",
        task=netmiko_send_command,
        command_string=f"{command}",
        enable=True
    )
    time_after = datetime.datetime.now().time()
    print(f"[{task.host.name}({message})] - Time before command was {time_before}, time after command was {time_after}")

    metrics = vp.parsed_extreme_vsp_show_int_gi_l1_interface(
        output.result)

    if METRIC_RESULTS in task.host.keys():
        task.host[METRIC_RESULTS] = merge(
            task.host[METRIC_RESULTS], metrics)
    else:
        task.host[METRIC_RESULTS] = metrics
