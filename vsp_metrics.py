#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
This script will retrieve some port statistics (29 values).
To run => ./main.py

It's an alternative to SNMP polling.
With SNMP, you will perform 44 SNMP_GET per port.

There are 82 ports an VSP 8K

=> env. 3600 SNMP_GET ...

This tool will only perform 7 (!) SSH commands.

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"


######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    from flask import Flask, Response, request
    app = Flask(__name__)
except ImportError as importError:
    print("Error import [vsp_metrics.py] flask")
    print(importError)

try:
    from nornir import InitNornir
    from nornir.core import Nornir
    from nornir.plugins.functions.text import print_result
except ImportError as importError:
    print("Error import [vsp_metrics.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json
except ImportError as importError:
    print("Error import [vsp_metrics.py] json")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import functions.nornir_fct as nf
except ImportError as importError:
    print("Error import [vsp_metrics.py] parsers.parsers")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Constantes
#
METRIC_RESULTS = 'metric_results'

######################################################
#
# Functions
#
def init_nornir() -> Nornir:
    nr = InitNornir(
        config_file="nornir/config.yml",
        logging={"file": "./nornir/nornir.log", "level": "debug"}
    )
    return nr

######################################################
#
# Flask Endpoints

# ----------------------------------------------------
#
#
@app.route("/int_errors", methods=['GET'])
def int_errors():
    """
    /int_errors endpoint
    """

    nr = init_nornir()

    devices = nr.filter(platform="extreme_vsp")
    print(f"Devices {devices.inventory.hosts}")

    results = devices.run(
        task=nf._int_errors,
        on_failed=True,
        num_workers=10
    )
    #print_result(results)

    all_results = dict()
    for device in devices.inventory.hosts:
        all_results = devices.inventory.hosts[device][METRIC_RESULTS]

    return Response(
        json.dumps(all_results),
        status=200,
        mimetype="application/json"
    )

# ----------------------------------------------------
#
#
@app.route("/int_vlacp_stats", methods=['GET'])
def int_vlacp_stats():
    """
    /int_vlacp_stats endpoint
    """

    nr = init_nornir()

    devices = nr.filter(platform="extreme_vsp")
    print(f"Devices {devices.inventory.hosts}")

    results = devices.run(
        task=nf._int_vlacp_stats,
        on_failed=True,
        num_workers=10
    )
    #print_result(results)

    all_results = dict()
    for device in devices.inventory.hosts:
        all_results = devices.inventory.hosts[device][METRIC_RESULTS]

    return Response(
        json.dumps(all_results),
        status=200,
        mimetype="application/json"
    )


# ----------------------------------------------------
#
#
@app.route("/int_all", methods=['GET'])
def int_all():
    """
    /int_all endpoint
    """

    nr = init_nornir()

    devices = nr.filter(platform="extreme_vsp")
    print(f"Devices {devices.inventory.hosts}")

    results = devices.run(
        task=nf._int_all,
        on_failed=True,
        num_workers=10
    )
    #print_result(results)

    all_results = dict()
    for device in devices.inventory.hosts:
        all_results = devices.inventory.hosts[device][METRIC_RESULTS]

    return Response(
        json.dumps(all_results),
        status=200,
        mimetype="application/json"
    )
######################################################
#
# Entry Point
#
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=55555, debug=True)
