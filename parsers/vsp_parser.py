#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
This script will retrieve some port statistics (29 values).
To run => ./main.py

It's an alternative to SNMP polling.
With SNMP, you will perform 44 SNMP_GET per port.

There are 82 ports an VSP 8K

=> env. 3600 SNMP_GET ...

This tool will only perform 7 (!) SSH commands.

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"


######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1


######################################################
#
# Import Library
#

try:
    import textfsm
except ImportError as importError:
    print("Error import [vsp_metrics.py] textfsm")
    print(importError)
    exit(EXIT_FAILURE)


######################################################
#
# Constantes
#
TEXTFSM_PATH = "./templates/textfsm/"
JINJA2_PATH = "./../templates/jinja2/"
DATA_VSP_PATH = "./../data/extreme_vsp/"

######################################################
#
# OPEN FILE Functions
#
def extract_data(template_name: str(), output: str()) -> list():

        template = open(TEXTFSM_PATH+template_name)
        results_template = textfsm.TextFSM(template)

        return results_template.ParseText(output)

def open_template_with_data(template_name: str(), output_file: str(), *, mode="r") -> list():

        file = open(DATA_VSP_PATH+output_file, mode)
        output = file.read()

        template = open(TEXTFSM_PATH+template_name)
        results_template = textfsm.TextFSM(template)

        parsed_results = results_template.ParseText(output)
        return parsed_results


######################################################
#
# Functions
#

# ----------------------------------------------------
#
# show interfaces gigabitEthernet error
#
def parsed_extreme_vsp_show_int_gi_error(output: str()) -> dict():

        result = dict()
        parsed_results = extract_data(
            'extreme_vsp_show_int_giga_error.template', output)

        for line in parsed_results:
                result[line[0]] = dict()
                result[line[0]]['error_align'] = line[1]
                result[line[0]]['error_fcs'] = line[2]
                result[line[0]]['frames_long'] = line[3]
                result[line[0]]['too_short'] = line[4]
                result[line[0]]['link_failure'] = line[5]
                result[line[0]]['carrier_sense'] = line[6]
                result[line[0]]['carrier_errors'] = line[7]
                result[line[0]]['sqtest_errors'] = line[8]
                result[line[0]]['in_discard'] = line[9]

        return result

# ----------------------------------------------------
#
# show interfaces gigabitEthernet statistics
#
def parsed_extreme_vsp_show_int_gi_statistics(output: str()) -> dict():

        result = dict()
        parsed_results = extract_data(
            'extreme_vsp_show_int_giga_statistics.template', output)

        for line in parsed_results:
                if line[0] not in result.keys():
                        result[line[0]] = dict()
                        result[line[0]]['in_octets'] = line[1]
                        result[line[0]]['out_octets'] = line[2]
                        result[line[0]]['in_packets'] = line[3]
                        result[line[0]]['out_packets'] = line[4]

                else:
                        result[line[0]]['in_flowctrl'] = line[1]
                        result[line[0]]['out_flowctrl'] = line[2]
                        result[line[0]]['in_pfc'] = line[3]
                        result[line[0]]['out_pfc'] = line[4]
                        result[line[0]]['outloss_packets'] = line[5]

        return result

# ----------------------------------------------------
#
# show interfaces gigabitEthernet statistics vlacp
#
def parsed_extreme_vsp_show_int_gi_statistics_vlacp(output: str()) -> dict():

        result = dict()
        parsed_results = extract_data(
            'extreme_vsp_show_int_giga_statistics_vlacp.template', output)

        for line in parsed_results:
                result[line[0]] = dict()
                result[line[0]]['vlacp_tx_vlacpdu'] = line[1]
                result[line[0]]['vlacp_rx_vlacpdu'] = line[2]
                result[line[0]]['vlacp_seqnum_mismatch'] = line[3]

        return result

# ----------------------------------------------------
#
# show interfaces gigabitEthernet statistics verbose
#
def parsed_extreme_vsp_show_int_gi_statistics_verbose(output: str()) -> dict():

        result = dict()
        parsed_results = extract_data(
            'extreme_vsp_show_int_giga_statistics_verbose.template', output)

        for line in parsed_results:
                result[line[0]]['in_unicast'] = line[1]
                result[line[0]]['out_unicast'] = line[2]
                result[line[0]]['in_multicast'] = line[3]
                result[line[0]]['out_multicast'] = line[4]
                result[line[0]]['in_brdcst'] = line[5]
                result[line[0]]['out_brdcst'] = line[6]
                result[line[0]]['in_lsm'] = line[7]
                result[line[0]]['out_lsm'] = line[8]

        return result

# ----------------------------------------------------
#
# show interfaces gigabitEthernet state
#
def parsed_extreme_vsp_show_int_gi_state(output: str()) -> dict():

        result = dict()
        parsed_results = extract_data(
            'extreme_vsp_show_int_giga_state.template', output)

        for line in parsed_results:
                result[line[0]] = dict()
                result[line[0]]['admin_status'] = line[1]
                result[line[0]]['port_state'] = line[2]
                result[line[0]]['state_reason'] = line[3]
                result[line[0]]['uptime_date'] = line[4]

        return result

# ----------------------------------------------------
#
# show interfaces gigabitEthernet l1-interface
#
def parsed_extreme_vsp_show_int_gi_l1_interface(output: str()) -> dict():

        result = dict()
        parsed_results = extract_data(
            'extreme_vsp_show_int_giga_l1-interface.template', output)

        for line in parsed_results:
                result[line[0]] = dict()
                result[line[0]]['auto_negotiate'] = line[1]
                result[line[0]]['duplex_admin'] = line[2]
                result[line[0]]['speed_admin'] = line[3]
                result[line[0]]['duplex_operate'] = line[4]
                result[line[0]]['speed_operate'] = line[5]

# ----------------------------------------------------
#
# show interfaces gigabitEthernet interface
#
def parsed_extreme_vsp_show_int_gi_interface(output: str()) -> dict():

        result = dict()
        parsed_results = extract_data(
            'extreme_vsp_show_int_giga_interface.template', output)

        for line in parsed_results:
                result[line[0]] = dict()
                result[line[0]]['interface_index'] = line[1]
                result[line[0]]['description_type'] = line[2]
                result[line[0]]['link_trap'] = line[3]
                result[line[0]]['port_lock'] = line[4]
                result[line[0]]['mtu'] = line[5]
                result[line[0]]['mac_address'] = line[6]

        return result
